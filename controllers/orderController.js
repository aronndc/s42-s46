const Order = require('../models/Order');
const User = require('../models/User');
const auth = require('../auth');

async function addOrder(req, res) {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)
    if (userData.isAdmin) {
        return res.status(401).send('Cannot order due to restriction! Login using a User account to order.')
    }
    let newOrder = new Order({
        userId: userData.id,
        products: [req.body.products],
        totalAmount: req.body.totalAmount
    })
    try {
        let order = await newOrder.save()
        if (order) {
            return res.send("Order Successfully Created!")
        }
        return res.send(false)
    } catch (error) {
        return res.send(error)
    }
};


async function getOrder(req, res) {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)
    if (userData.isAdmin) {
        return res.status(401).send('Cannot order due to restriction! Login using a User account to order.')
    }
    try {
        const order = await Order.findById(req.params.orderId)
        if (order) {
            return res.send(order)
        }
        const userOrder = await Order.find({ _id: req.params.orderId, userId: user.id })
        if (userOrder.length === 0) {
            return res.send('No order made')
        }
        res.send(order)
    } catch (error) {
        return res.send(error)
    }
};


async function getAllOrders(req, res) {
    const userData = auth.decode(req.headers.authorization)
    try {
        if (!userData.isAdmin) {
            return res.status(401).send('Only an Admin is authorized to get all Order Details.')
        }
        const orders = await Order.find({})
        if (orders) {
            return res.send(orders)
        }
        const userOrders = await Order.find({ userId: user.id })
        res.send(userOrders)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};


module.exports = {
    addOrder: addOrder,
    getOrder: getOrder,
    getAllOrders: getAllOrders
}