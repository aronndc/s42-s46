const Product = require('../models/Product');
const auth = require('../auth');


async function addProduct(req, res){
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)
    if(!userData.isAdmin){
        return res.status(401).send("Unable to add product due to User restriction!")
    }

    let newProduct = new Product({
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        category: req.body.category,
        image: req.body.image
    })
    try{
        const product = await newProduct.save()
        if(product){
            return res.send(`Product ${req.body.name} created`)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function getActiveProducts(req, res){
    try{
        const product = await Product.find({isActive: true})

        if(product && product.length > 0){
            return res.send(product)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function retrieveProduct(req, res){
    try{
        const product = await Product.findById(req.params.productId)
        if(product){
            return res.send(product)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function productUpdate(req, res){
    const userData = auth.decode(req.headers.authorization)

    const updatedProduct = {
        name: req.body.name,
        price: req.body.price,
        description: req.body.description
    }
    try{
        if(!userData.isAdmin){
            return res.status(401).send("Unable to update product due to User restriction!")
        }
        const result = await Product.findByIdAndUpdate( req.params.productId, updatedProduct)

        if(result){
            return res.send("Product Successfully Updated!")
        }
        return res.send(false)
        console.log(error)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function archiveProduct(req, res){
    const userData = auth.decode(req.headers.authorization)

    try{
        if(!userData.isAdmin){
            return res.send("Unable to archive product due to User restriction!")
        }
        const result = await Product.findByIdAndUpdate(req.params.productId, {isActive: req.body.isActive})

        if(result){
            return res.send('Successfully archived the product!')
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

module.exports = {
    addProduct: addProduct,
    getActiveProducts: getActiveProducts,
    retrieveProduct: retrieveProduct,
    productUpdate: productUpdate,
    archiveProduct: archiveProduct
}