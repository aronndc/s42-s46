const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

async function registerUser(req,res){
    const hashedPassword = bcrypt.hashSync(req.body.password, 10)
    try{
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            mobileNo: req.body.mobileNo,
            password: hashedPassword
        })
        const result = await newUser.save()
        if(result){
            return res.send("Account registration successful!")
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send("Account already exists!")
    }
};

async function loginUser(req, res){
    try{
        const user = await User.findOne({email: req.body.email})
        if(user == null){
            return res.send("User not found!");
        }
        const isPasswordCorrect = bcrypt.compareSync(
            req.body.password,
            user.password
        )
        if(isPasswordCorrect){
            return res.send({access: auth.createAccessToken(user)})
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function getProfile(req, res){
    const userData = auth.decode(req.headers.authorization)
    console.log(userData.id)
    try{
        const user = await User.findById(userData.id, {password:0})
        console.log(user);
        if(user){
            return res.send(user)
        }
        return res.send("User not found!")
    }catch(error){
        console.log(error)
        res.send(error)
    }
};

async function updateUser(req, res){
    const userData = auth.decode(req.headers.authorization)
    try{
        if(!userData.isAdmin){
            return res.send("User is not Authorized")
        }
        const result = await User.findByIdAndUpdate(req.body.userId, {isAdmin: req.body.isAdmin})

        if(result){
            return res.send(true)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

module.exports = {
    registerUser: registerUser,
    loginUser: loginUser,
    getProfile: getProfile,
    updateUser: updateUser
}