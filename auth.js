const jwt = require("jsonwebtoken");

const secret = "CapstoneTwoSecretKey";

function createAccessToken(user) {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret, {});
}

function verify(req, res, next) {
  let token = req.headers.authorization;
  console.log(token);
  if (typeof token !== "undefined") {
    token = token.slice(7, token.lenth);
    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return res.send('Encountered and error! Please try again.');
      }
      next();
    });
  } else {
    return res.send('Encountered and error! Please try again.');
  }
}

function decode(token){
  if(typeof token !== "undefined"){
    token = token.slice(7, token.lenth);
    return jwt.verify(token, secret,(error,data)=>{
      if(error){
        return null
      }
      return jwt.decode(token,{complete: true}.payload);
    })
  }else{
    return null;
  }
}

module.exports = {
  createAccessToken: createAccessToken,
  verify: verify,
  decode: decode
};
