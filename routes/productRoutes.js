const express = require('express');
const productController = require('../controllers/productController');
const auth = require('../auth');

const router = express.Router();

// Create a new Product (Admin Only)
router.post('/', auth.verify, productController.addProduct);
// Retrieve all Product
router.get('/getactiveproducts', productController.getActiveProducts);
// Retrieve Product
router.get('/:productId/retrieve', productController.retrieveProduct);
// Update Product information (Admin Only)
router.put('/:productId', auth.verify, productController.productUpdate);
// Archive Product (Admin Only)
router.put('/:productId/archive', auth.verify, productController.archiveProduct);


module.exports = router;