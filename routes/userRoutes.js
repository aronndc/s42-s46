const express = require('express');
const userController = require('../controllers/userController');
const auth = require('../auth');

const router = express.Router();

//User Registration
router.post('/register', userController.registerUser);
//User Authentication
router.post('/login', userController.loginUser);
// retrieve user Details
router.get('/details', auth.verify, userController.getProfile);


module.exports = router;