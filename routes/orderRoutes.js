const express = require('express')
const orderController = require('../controllers/orderController');
const auth = require('../auth');
// const productController = require('./product.routes');

const router = express.Router();

// User Create Order
router.post('/', auth.verify, orderController.addOrder);
// Retrieve User Order
router.get('/:orderId', orderController.getOrder);
// retrieve all Orders (Admin)
router.get('/', auth.verify, orderController.getAllOrders);

module.exports = router;